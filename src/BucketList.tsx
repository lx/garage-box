import React from 'react';
import { S3Client, ListBucketsCommand } from '@aws-sdk/client-s3';

import Breadcrumb from 'react-bootstrap/Breadcrumb';
import Container from 'react-bootstrap/Container';
import ListGroup from 'react-bootstrap/ListGroup';
import { LinkContainer } from 'react-router-bootstrap'

import { BsBucket } from 'react-icons/bs';


type Props = {
  client: S3Client;
};

type State = {
  buckets: string[];
};

class BucketList extends React.Component<Props, State> {
  state = {
    buckets: [],
  };

  async componentDidMount() {
    let command = new ListBucketsCommand({});
    try {
      const data = await this.props.client.send(command);
      console.log("ok", data);
      const buckets = (data.Buckets || []).map((b) => (b.Name || 'aza'));
      buckets.sort((a, b) => a.localeCompare(b, undefined, {sensitivity: 'base'}));
      this.setState({buckets: buckets});
    } catch(error) {
      console.log("err", error);
    }
  }

  render() {
    return (
      <>
        <Container className="pb-3">
          <Breadcrumb>
            <Breadcrumb.Item active>my buckets</Breadcrumb.Item>
          </Breadcrumb>
        </Container>
        <ListGroup>
          { this.state.buckets.map((b) =>
            <LinkContainer to={ "/" + b }>
              <ListGroup.Item key={ b } action>
                <BsBucket /> { b }
              </ListGroup.Item>
            </LinkContainer>
          )}
        </ListGroup>
      </>
    );
  }

}

export default BucketList;
