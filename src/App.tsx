import React from 'react';
import { S3Client } from '@aws-sdk/client-s3';
import {
  HashRouter,
  Routes,
  Route,
} from "react-router-dom";
import BucketList from './BucketList';
import { ObjectList1, ObjectList2 } from './ObjectList';
import './App.css';

import Container from 'react-bootstrap/Container';
import Form from 'react-bootstrap/Form';
import Button from 'react-bootstrap/Button';

type AppProps = {};

type AppState = {
  accessKeyId: string;
  secretAccessKey: string;
  client: S3Client | null;
};

class App extends React.Component<AppProps, AppState> {
  state = {
    accessKeyId: "GK31c2f218a2e44f485b94239e",
    secretAccessKey: "b892c0665f0ada8a4755dae98baa3b133590e11dae3bcc1f9d769d67f16c3835",
    client: null,
  };

  handleChangeAccessKeyId = (e: React.ChangeEvent<HTMLInputElement>): void => {
    this.setState({accessKeyId: e.currentTarget.value});
  }

  handleChangeSecreteAccessKey = (e: React.ChangeEvent<HTMLInputElement>): void => {
    this.setState({secretAccessKey: e.currentTarget.value});
  }

  login = async (e: React.SyntheticEvent) => {
    e.preventDefault();
    console.log("Login with: ", this.state);
    let c = new S3Client({
      //endpoint: 'https://garage-staging.home.adnab.me',
      logger: console,
      // Necessary to write the full endpoint struct instead of endpoint: 'http://localhost:3811'
      // otherwise AWS SDK removes the port number from the signature, which is a bug:
      // https://github.com/aws/aws-sdk-js-v3/issues/1941
      endpoint: {
        protocol: 'http',
        hostname: 'localhost:3811',
        path: '/',
      },
      tls: false,
      credentials: {
          accessKeyId: this.state.accessKeyId,
          secretAccessKey: this.state.secretAccessKey,
      },
      forcePathStyle: true,
      region: 'us-east-1',
    });

    this.setState({client: c});
  };

  render() {
    if (this.state.client) {
      return (
        <Container className="p-3">
          <HashRouter>
            <Routes>
              <Route path="/:bucket/*" element={ <ObjectList2 client={this.state.client} /> } />
              <Route path="/:bucket" element={ <ObjectList1 client={this.state.client} /> } />
              <Route path="/" element={<BucketList client={this.state.client} />} />
            </Routes>
          </HashRouter>
        </Container>
      );
    } else {
      return (
        <Container className="p-3">
          <Form onSubmit={this.login} >

            <Form.Group className="mb-3" controlId="formAccessKeyId">
              <Form.Label>Access key ID</Form.Label>
              <Form.Control type="text" placeholder="Access key id" value={ this.state.accessKeyId } onChange={this.handleChangeAccessKeyId} />
            </Form.Group>

            <Form.Group className="mb-3" controlId="formSecretAccessKey">
              <Form.Label>Secret access key</Form.Label>
              <Form.Control type="text" placeholder="Secret access key" value={ this.state.secretAccessKey } onChange={this.handleChangeSecreteAccessKey} />
            </Form.Group>

            <Button variant="primary" type="submit" onClick={this.login}>
              Log in
            </Button>
          </Form>
        </Container>
      );
    }
  }
}


export default App;
